package mMahdiDh.therickandmorty.ui.screen

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import mMahdiDh.therickandmorty.databinding.FragmentCharactersBinding
import mMahdiDh.therickandmorty.ui.CharactersRecyclerViewAdapter
import mMahdiDh.therickandmorty.util.model.repository.Respond
import mMahdiDh.therickandmorty.util.model.repository.remote.Result
import mMahdiDh.therickandmorty.util.model.ui.MyDiffUtil

class CharactersFragment : Fragment() {

    companion object {
        fun newInstance() = CharactersFragment()
    }

    private lateinit var viewModel: CharactersViewModel
    private lateinit var binding: FragmentCharactersBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCharactersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(CharactersViewModel::class.java)

        viewModel.pageStabilizer()

        onBindingRecyclerView(CharactersRecyclerViewAdapter(MyDiffUtil()))
    }

    private fun onBindingRecyclerView(recyclerViewAdaptor: CharactersRecyclerViewAdapter<Result>) {
        binding.charactersRclr.apply {
            this.adapter = recyclerViewAdaptor
            this.layoutManager = LinearLayoutManager(activity)
        }

        viewModel.charactersList.observe(viewLifecycleOwner, Observer {  respond ->
            when (respond) {
                is Respond.Loading -> {
                    showProgressBar()
                }
                is Respond.Success -> {
                    recyclerViewAdaptor.submitList(respond.get() as List<Result>)
                    hideProgressBar()
                }
                is Respond.Error -> {
                    hideProgressBar()
                    Toast.makeText(context,"${respond.get()}", Toast.LENGTH_LONG).show()
                }
            }
        })

        viewModel.getCharacters(viewModel.nextPage())

    }

    private fun showProgressBar() {
        binding.charactersRclr.visibility = View.GONE
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
        binding.charactersRclr.visibility = View.VISIBLE
    }

}

