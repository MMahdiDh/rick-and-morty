package mMahdiDh.therickandmorty.ui.screen

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import mMahdiDh.therickandmorty.databinding.FragmentCharacterBinding


class CharacterFragment : Fragment() {

    companion object {
        fun newInstance() = CharacterFragment()
    }

    private lateinit var viewModel: CharacterViewModel
    private lateinit var binding: FragmentCharacterBinding

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        binding = FragmentCharacterBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(CharacterViewModel::class.java)

        val args = CharacterFragmentArgs.fromBundle(requireArguments())

        binding. apply {
            Glide.with(root.context).load(args.imageUrl).into(dCharacterImg)
            dNameTxt.text = args.name
            dGenderTxt.text = args.gender
            dSpeciesTxt.text = args.species
            dStatusTxt.text = args.states
            dCreatedTxt.text = args.created
            dOriginTxt.text = args.origin
            dEpisodesTxt.text = args.episodesNo.toString()
        }

    }

}