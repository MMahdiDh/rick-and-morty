package mMahdiDh.therickandmorty.ui.screen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import mMahdiDh.therickandmorty.repository.getCharactersRetrofit
import mMahdiDh.therickandmorty.util.model.repository.Respond

class CharactersViewModel : ViewModel() {
    private val _charactersList: MutableLiveData<Respond<*>> =
            MutableLiveData(Respond.Loading<Any>())

    val charactersList: LiveData<Respond<*>>
        get() = _charactersList

    private var _pageNo = 0

    private var _isNextPage: Boolean = true

    fun isNextPage() = _isNextPage

    fun nextPage() = this._pageNo+1

    fun prePage() = this._pageNo-1

    fun pageStabilizer() { if (this._pageNo > 0) this._pageNo -- }

    fun getCharacters(action: Int) {
        viewModelScope.launch {
            _charactersList.value = Respond.Loading<Any>()

            val respond = getCharactersRetrofit(action)

            _charactersList.value = if (respond is Respond.Success) {

                Respond.Success(respond.get().results).also {

                    this@CharactersViewModel._pageNo = action

                    this@CharactersViewModel._isNextPage =
                            if (respond.get().info.next == null) false else true
                }

            } else { respond }
        }
    }
}