package mMahdiDh.therickandmorty.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import mMahdiDh.therickandmorty.databinding.ItemCharacterBinding
import mMahdiDh.therickandmorty.ui.screen.CharactersFragmentDirections
import mMahdiDh.therickandmorty.util.model.ui.MyDiffUtil
import mMahdiDh.therickandmorty.util.model.repository.remote.Result

class CharactersRecyclerViewAdapter<T: Result>(myDiffUtil: MyDiffUtil<T>): ListAdapter<T, CharactersRecyclerViewAdapter.MyViewHolder>(myDiffUtil) {
    class MyViewHolder(
            private val _binding: ItemCharacterBinding
    ): RecyclerView.ViewHolder(_binding.root) {
        fun binding(dataset: Result) = _binding.apply {
            Glide.with(_binding.root).load(dataset.image).into(characterImg)
            nameTxt.text = dataset.name
            genderTxt.text = dataset.gender
            speciesTxt.text = dataset.species
            statusTxt.text = dataset.status

            characterImg.setOnClickListener {
                it.findNavController().navigate(
                    CharactersFragmentDirections.actionCharactersFragmentToCharacterFragment(
                        dataset.image,
                        dataset.name,
                        dataset.gender,
                        dataset.species,
                        dataset.status,
                        dataset.created,
                        dataset.origin.name,
                        dataset.episode.size
                    )
                )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder = MyViewHolder(
            ItemCharacterBinding.inflate(LayoutInflater.from(parent.context))
    )

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding(getItem(position))
    }
}