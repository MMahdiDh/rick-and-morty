package mMahdiDh.therickandmorty.repository

import mMahdiDh.therickandmorty.repository.remote.retrofit
import mMahdiDh.therickandmorty.util.LOG_TAG
import mMahdiDh.therickandmorty.util.model.repository.Respond
import retrofit2.Response


fun <T> handelDataRespond(response: Response<T>): Respond<T> =
    if (response.isSuccessful) {
        try {
            Respond.Success(response.body()!!)
        } catch (e: Error) {
            Respond.Error("$LOG_TAG : List is empety!!! => ${response.body()!!}")
        }
    } else {
        Respond.Error(response.message())
    }

suspend fun getCharactersRetrofit(pageNo: Int) = handelDataRespond(
        retrofit.rickAndMortyApi.getCharacter(pageNo)
)
