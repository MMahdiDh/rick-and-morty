package mMahdiDh.therickandmorty.repository.remote

import mMahdiDh.therickandmorty.util.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object retrofit {
    private val _retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()

    val rickAndMortyApi = this._retrofit.create(RickAndMortyApi::class.java)
}