package mMahdiDh.therickandmorty.repository.remote

import mMahdiDh.therickandmorty.util.model.repository.remote.CharacteresList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RickAndMortyApi {
    @GET("character")
    suspend fun getCharacter(@Query("page") pageNo: Int): Response<CharacteresList>
}