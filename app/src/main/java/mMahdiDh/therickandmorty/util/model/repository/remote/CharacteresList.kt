package mMahdiDh.therickandmorty.util.model.repository.remote


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CharacteresList(
    @Json(name = "info")
    val info: Info,
    @Json(name = "results")
    val results: List<Result>
)