package mMahdiDh.therickandmorty.util.model.ui

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil
import mMahdiDh.therickandmorty.util.model.repository.remote.Result

class MyDiffUtil<T: Result>: DiffUtil.ItemCallback<T>(){
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean =
        oldItem.id == newItem.id

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean =
        oldItem == newItem
}