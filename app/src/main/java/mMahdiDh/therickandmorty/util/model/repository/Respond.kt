package mMahdiDh.therickandmorty.util.model.repository


sealed class Respond<T> {
    abstract fun get(): Any?
    class Loading<T>() : Respond<T>() {
        override fun get() = null
    }

    class Success<T>(val data: T) : Respond<T>() {
        override fun get() = data
    }

    class Error<T>(val message: String) : Respond<T>() {
        override fun get() = message
    }
}